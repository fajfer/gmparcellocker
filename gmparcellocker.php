<?php

/**
 * Parcel Locker Module
 *
 * @package   gmparcellocker
 * @author    Dariusz Tryba (contact@greenmousestudio.com)
 * @copyright Copyright (c) Green Mouse Studio (http://www.greenmousestudio.com)
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (!defined('_PS_VERSION_')) {
    exit;
}
if (!class_exists('BaseLinkerOrder')) {
    require_once(_PS_MODULE_DIR_ . 'gmparcellocker/classes/BaseLinkerOrder.php');
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class GmParcelLocker extends Module implements WidgetInterface {

    public function __construct() {
        $this->name = 'gmparcellocker';
        $this->prefix = strtoupper($this->name);
        $this->tab = 'shipping_logistics';
        $this->version = '1.4.0';
        $this->author = 'GreenMouseStudio.com';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Parcel locker');
        $this->description = $this->l('Allows the customer to chose a parcel locker in the cart');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:gmparcellocker/views/templates/choice.tpl';
    }

    public function install() {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (!parent::install() || !$this->registerHook('displayCarrierExtraContent') || !$this->installDb() || !$this->createCarriers() || !$this->registerHook('header') || !$this->registerHook('actionValidateOrder') || !$this->registerHook('sendMailAlterTemplateVars') || !$this->registerHook('addWebserviceResources')
        ) {
            Configuration::updateValue('GMPARCELLOCKER_REPLACE', false);
            Configuration::updateValue('GMPARCELLOCKER_DISPLAY', 'modal');
            return false;
        }
        return true;
    }

    public function getContent() {
        $content = '';
        $content .= '<p class="alert alert-warning">' . $this->l('Remember to configure and activate the new Parcel Locker carrier created by the module') . '</p>';
        if (Tools::isSubmit('submit' . $this->name)) {
            Configuration::updateValue('GMPARCELLOCKER_REPLACE', (int) Tools::getValue('GMPARCELLOCKER_REPLACE'));
            Configuration::updateValue('GMPARCELLOCKER_DISPLAY', Tools::getValue('GMPARCELLOCKER_DISPLAY'));
            $content .= $this->displayConfirmation($this->l('Settings updated'));
        }
        $content .= $this->renderForm();
        return $content . $this->context->smarty->fetch($this->local_path . 'views/templates/admin/gms.tpl');
    }

    public function renderForm() {
        $displays = array(
            array(
                'id' => 'modal',
                'name' => $this->l('Modal')
            ),
            array(
                'id' => 'dropdown',
                'name' => $this->l('Dropdown')
            ),
        );
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Replace delivery address with parcel locker address'),
                        'name' => 'GMPARCELLOCKER_REPLACE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No'),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Display type'),
                        'name' => 'GMPARCELLOCKER_DISPLAY',
                        'options' => array(
                            'query' => $displays,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit' . $this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab
                . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues() {
        return array(
            'GMPARCELLOCKER_REPLACE' => (bool) Tools::getValue('GMPARCELLOCKER_REPLACE', Configuration::get('GMPARCELLOCKER_REPLACE')),
            'GMPARCELLOCKER_DISPLAY' => Tools::getValue('GMPARCELLOCKER_DISPLAY', Configuration::get('GMPARCELLOCKER_DISPLAY')),
        );
    }

    public function displayInfoByCart($cartId) {
        $pointData = $this->getPointDataForCart($cartId);
        $this->context->smarty->assign('gmChosenParcel', $pointData);
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/parcel.tpl');
    }

    protected function createCarriers() {
        $carrier = new Carrier();
        $carrier->name = $this->l('Parcel Locker');
        $carrier->active = 0;
        $carrier->is_free = 0;
        $carrier->shipping_handling = 1;
        $carrier->shipping_external = 0;
        $carrier->shipping_method = 1;
        $carrier->max_width = 0;
        $carrier->max_height = 0;
        $carrier->max_depth = 0;
        $carrier->max_weight = 0;
        $carrier->grade = 0;
        $carrier->is_module = 1;
        $carrier->need_range = 1;
        $carrier->range_behavior = 1;
        $carrier->external_module_name = $this->name;
        $carrier->url = 'https://inpost.pl/sledzenie-przesylek?number=@';

        $delay = array();

        foreach (Language::getLanguages(false) as $language) {
            $delay[$language['id_lang']] = $this->l('Parcel Locker');
        }

        $carrier->delay = $delay;
        if (!$carrier->save()) {
            return false;
        }
        $groups = [];
        foreach (Group::getGroups((int) Context::getContext()->language->id) as $group) {
            $groups[] = $group['id_group'];
        }
        if (!$carrier->setGroups($groups)) {
            return false;
        }
        return true;
    }

    protected function installDb() {
        return Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'gmparcellocker` (
			`id_cart` int(10) unsigned NOT NULL,
            `parcel` varchar(128),
            PRIMARY KEY (`id_cart`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' default CHARSET=utf8');
    }

    public function uninstall() {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'gmparcellocker`');
        Configuration::deleteByName('GMPARCELLOCKER_REPLACE');
        Configuration::deleteByName('GMPARCELLOCKER_DISPLAY');
        return parent::uninstall();
    }

    public function hookDisplayCarrierExtraContent($params) {
        $cart = $this->context->cart;
        //$deliveryAddress = new Address($cart->id_address_delivery);
        //$searchAddress = $deliveryAddress->address1 . ' ' . $deliveryAddress->postcode . ' ' . $deliveryAddress->city;
        $pointData = $this->getPointDataForCart($cart->id);
        $this->context->smarty->assign([
            'gmChosenParcel' => $pointData,
                //'gmSearchAddress' => $searchAddress
        ]);
        $displayOption = Configuration::get('GMPARCELLOCKER_DISPLAY');
        if ($displayOption == 'dropdown') {
            return $this->context->smarty->fetch($this->local_path . 'views/templates/dropdown.tpl');
        } else {
            return $this->context->smarty->fetch($this->local_path . 'views/templates/button.tpl');
        }
    }

    public function getParcelAddressForCart($cartId) {
        $pointData = $this->getPointDataForCart($cartId, true);
        $parts = explode('|', $pointData);
        $address = [
            'parcel_name' => $parts[0],
            'address' => $parts[1],
            'postcode' => substr(trim($parts[2]), 0, 6),
            'city' => trim(substr($parts[2], 7))
        ];
        return $address;
    }

    public function getPointDataForCart($cartId, $raw = false) {
        $pointData = Db::getInstance()->getValue('SELECT `parcel` FROM `' . _DB_PREFIX_ . 'gmparcellocker` WHERE `id_cart` = ' . $cartId);
        if ($pointData) {
            if (!$raw) {
                $pointData = str_replace('|', ',', $pointData);
            }
            return $pointData;
        }
        return '---';
    }

    public function hookHeader() {
        if ($this->context->controller->php_self === 'order' || Tools::getValue('controller') == 'supercheckout' || Tools::getValue('controller') == 'checkout') {
            $cartId = Context::getContext()->cart->id;
            Media::addJsDef(['gmCartId' => $cartId,
                'gmParcelLockerAjaxUrl' => $this->context->link->getModuleLink('gmparcellocker', 'ajax', array(), null,
                        null, null, true),]);
            $this->context->controller->registerStylesheet('gmparcellocker-css',
                    'modules/' . $this->name . '/views/css/gmparcellocker.css',
                    [
                        'media' => 'all',
                        'priority' => 200,
            ]);

            $displayOption = Configuration::get('GMPARCELLOCKER_DISPLAY');
            if ($displayOption == 'dropdown') {
                $this->context->controller->registerJavascript('gmparcellocker-js130-dropdown',
                        'modules/' . $this->name . '/views/js/gmparcellocker-dropdown.js',
                        [
                            'position' => 'bottom',
                            'priority' => 150
                ]);
            } else {
                $this->context->controller->registerJavascript('gmparcellocker-js130',
                        'modules/' . $this->name . '/views/js/gmparcellocker.js',
                        [
                            'position' => 'bottom',
                            'priority' => 150
                ]);
            }
            return '<script src="https://geowidget.easypack24.net/js/sdk-for-javascript.js"></script>
                <link rel="stylesheet" href="https://geowidget.easypack24.net/css/easypack.css"/>';
        }
    }

    public function hookActionValidateOrder($params) {
        if ((bool) Configuration::get('GMPARCELLOCKER_REPLACE')) {
            $order = $params['order'];
            $cart = $params['cart'];
            $carrier = new Carrier($order->id_carrier);
            if ($carrier->external_module_name == $this->name) {
                try {
                    $current = new Address($order->id_address_delivery);
                    $a = new Address();
                    $parcelAddress = $this->getParcelAddressForCart($cart->id);
                    $a->alias = $parcelAddress['parcel_name'];
                    $a->id_customer = $current->id_customer;
                    $a->id_country = $current->id_country;
                    $a->firstname = $this->l('Parcel');
                    $a->lastname = '.';
                    $a->address1 = $parcelAddress['address'];
                    if (strlen($current->address2) > 0) {
                        $a->address2 = '.';
                    }
                    $a->city = $parcelAddress['city'];
                    $a->postcode = $parcelAddress['postcode'];
                    $a->phone = $current->phone;
                    $a->phone_mobile = $current->phone_mobile;
                    $a->deleted = true;
                    $a->save();
                    $this->forceParcelName($a->id, $parcelAddress['parcel_name']);
                    $order->id_address_delivery = $a->id;
                    $order->update();
                } catch (Exception $e) {
                    PrestaShopLogger::addLog($e->getMessage(), 3);
                }
            }
        }
    }

    protected function forceParcelName($addressId, $parcelName) {
        Db::getInstance()->update('address', ['lastname' => $parcelName], 'id_address = ' . (int) $addressId);
    }

    public function hookSendMailAlterTemplateVars($params) {
        $template = $params['template'];
        $vars = &$params['template_vars'];
        $vars['{gmparcellocker}'] = '';
        if (in_array($template, ['order_conf', 'new_order'])) {
            if (stripos($vars['{carrier}'], 'paczkomat') !== false) {
                $cartId = $this->context->cart->id;
                $chosenParcel = $this->getPointDataForCart($cartId);
                $vars['{gmparcellocker}'] = $chosenParcel;
            }
        }
    }

    public function hookAddWebserviceResources() {
        return array(
            'bl_order' => array('description' => 'Extended order data for use by BaseLinker', 'class' => 'BaseLinkerOrder'),
        );
    }

    public function getWidgetVariables($hookName, array $configuration) {
        $cartId = $this->context->cart->id;
        if (!$cartId) {
            $cartId = Tools::getValue('id_cart');
        }
        if (!$cartId) {
            return false;
        }
        $cart = new Cart($cartId);
        $carrier = new Carrier($cart->id_carrier);
        if ($carrier->external_module_name == $this->name) {
            return array(
                'gmChosenParcel' => $this->getPointDataForCart($cartId)
            );
        }
        return false;
    }

    public function renderWidget($hookName, array $configuration) {
        $variables = $this->getWidgetVariables($hookName, $configuration);
        if (empty($variables)) {
            return false;
        }
        $this->smarty->assign($variables);
        return $this->fetch($this->templateFile);
    }

}