<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_c5824aabdfcfe98d5950818bab0260e6'] = 'Paczkomaty 24/7';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_81f5ab314898589259232be18d0c96de'] = 'Umożliwia wybór paczkomatu w koszyku';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_24d11524546e70303f21a814b7b2388d'] = 'Pamiętaj, aby skonfigurować i aktywować nowego przewoźnika Paczkomaty 24/7, utworzonego przez ten moduł';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_c888438d14855d7d96a2724ee9c306bd'] = 'Zapisano ustawienia';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_c59d6df802c32f037c2a15ff75faec17'] = 'Okienko modal';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_7498c445a737312f3678aa1494e01a38'] = 'Lista rozwijana';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_955c8f1a0a5b569d549874531a6d2223'] = 'Zamień adres dostawy na adres paczkomatu';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_93cba07454f06a4a960172bbd6e2a435'] = 'Tak';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nie';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_365119d429fc230c8e9b5c3c94842c19'] = 'Sposób wyświetlania';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_39b6fda48dbec22f92c82517456047c2'] = 'Paczkomaty 24/7';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_f38e9081d395bb971dd1bf81f65f22e6'] = 'Paczkomat';
$_MODULE['<{gmparcellocker}prestashop>button_04f689478e79ad944c1893dc7e713810'] = 'Wybrany paczkomat:';
$_MODULE['<{gmparcellocker}prestashop>button_494bd131a34e4dc02be773266700e935'] = 'Wybierz paczkomat';
$_MODULE['<{gmparcellocker}prestashop>dropdown_04f689478e79ad944c1893dc7e713810'] = 'Wybrany paczkomat';
$_MODULE['<{gmparcellocker}prestashop>choice_04f689478e79ad944c1893dc7e713810'] = 'Wybrany Paczkomat:';
