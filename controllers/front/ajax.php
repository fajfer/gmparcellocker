<?php

class GmParcelLockerAjaxModuleFrontController extends ModuleFrontController {

    public function initContent() {
        ob_end_clean();
        header('Content-Type: application/json');

        $cartId = (int) Tools::getValue('cartId');
        $pointData = trim(Tools::getValue('pointData'));
        $db = Db::getInstance();
        $storedPoint = $db->getValue('SELECT `parcel` FROM `' . _DB_PREFIX_ . 'gmparcellocker` WHERE `id_cart` = ' . $cartId);
        if ($storedPoint !== false) {
            $res = $db->update('gmparcellocker', ['parcel' => pSQL($pointData)], 'id_cart = ' . $cartId);
        } else {
            $res = $db->insert('gmparcellocker', ['parcel' => pSQL($pointData), 'id_cart' => $cartId]);
        }
        if ($res) {
            $msg = 'OK';
        } else {
            $msg = 'Error';
        }
        die(json_encode([
            'msg' => $msg
        ]));
    }

}
