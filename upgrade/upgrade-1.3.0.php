<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_3_0($object) {
    Configuration::updateValue('GMPARCELLOCKER_DISPLAY', 'modal');
    return true;
}
