<?php

class BaseLinkerOrder extends Order {

    public $bl_delivery_point_name;
    public $bl_delivery_point_address;
    public $bl_delivery_point_city;
    public $bl_delivery_point_postcode;

    public function __construct($id = null, $idLang = null) {
        self::$definition = parent::$definition;
        self::$definition['fields']['bl_delivery_point_name'] = array('type' => self::TYPE_STRING);
        self::$definition['fields']['bl_delivery_point_address'] = array('type' => self::TYPE_STRING);
        self::$definition['fields']['bl_delivery_point_city'] = array('type' => self::TYPE_STRING);
        self::$definition['fields']['bl_delivery_point_postcode'] = array('type' => self::TYPE_STRING);
        parent::__construct($id, $idLang);

        if ($id) {
            if (!$idLang) {
                $idLang = Configuration::get('PS_LANG_DEFAULT');
            }
            $carrier = new Carrier($this->id_carrier);
            if ($carrier->external_module_name == 'gmparcellocker') {
                $m = Module::getInstanceByName('gmparcellocker');
                $parcel = $m->getParcelAddressForCart((int) $this->id_cart);
                $this->bl_delivery_point_name = $parcel['parcel_name'];
                $this->bl_delivery_point_address = $parcel['address'];
                $this->bl_delivery_point_postcode = $parcel['postcode'];
                $this->bl_delivery_point_city = $parcel['city'];
            }
            if ($carrier->external_module_name == 'gmpickup') {
                $m = Module::getInstanceByName('gmpickup');
                $storeId = $m->getSelectedStoreIdForCart((int) $this->id_cart);
                $store = new Store($storeId, $idLang);
                $this->bl_delivery_point_name = $store->name;
                $this->bl_delivery_point_address = $store->address1.' '.$store->address2;
                $this->bl_delivery_point_postcode = $store->postcode;
                $this->bl_delivery_point_city = $store->city;
            }
        }
    }

}
